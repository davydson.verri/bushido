# Bushido

"Bushido" é um termo da língua japonesa que significa literalmente "caminho do guerreiro" (do japonês "bushi" = guerreiro; "do" = caminho). Este correspondia a um código de honra subentendido, não escrito e que de certa forma ditava o modo de vida dos guerreiros samurais do Japão feudal.

E nós da equipe de engenharia do grupo Mutual temos o nosso Bushido

## Entender e questionar

Qual é o propósito? Quem vamos ajudar? Que números iremos melhorar? Isso faz sentido? Isso é mesmo prioridade?

## Padronizar e componentizar

Cada problema deve ter apenas uma solução. A padronização e componentização são essenciais para garantir a velocidade, qualidade, flexibilidade, reaproveitamento.

## Desenvolver

Garantir a flexibilidade, testabilidade, escalabilidade e manutenibilidade do software.

"Utilize todo o seu esforço e inteligência para criar soluções sólidas. Você não terá tempo para refazer!"

## Corrigir

Qualquer bug deve ser corrigido imediatamente (na raiz). O nosso cliente não merece esperar!

## Contribuir

Ajudar o time a conquistar resultados extraordinários. #TEAMPLAY

"Jogar sozinho não garante a vitória do campeonato."

## Organizar

Manter o taskboard atualizado. Definir corretamente as estimativas, esforço e atividade das tarefas.

"Sem organização, não há otimização."

## Monitorar

Acompanhar os relatórios de performance, uptime, alertas de erro, etc. Nunca se omitir!

"É um enorme CONSTRANGIMENTO descobrir problemas através dos clientes."

## Ajudar

Dar suporte aos demais times respeitando o SLA combinado.
